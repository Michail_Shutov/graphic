# coding:utf-8

import os

from filters import filters_api
from filters.base import FilterError, INT, FILE, SELECT
from functools import partial
from singleton_decorator import singleton
from threading import Thread
from PIL import Image, ImageTk

# for GraphicConsoleAPI
from termcolor import colored

# for GraphicTkinerAPI
from tkinter import Tk, Frame, Label, Button, Entry, Listbox, LabelFrame, \
    filedialog, X, Y, W, N, E, S, BOTH, YES, FALSE
from tkinter.messagebox import showerror, showinfo


class GraphicAPI:
    r"""
    Работа с изображениями по курсу компьютерной графики (встроенные функции в PIL не используются
    осознанно, цель - понять реализацию основных алгоритмов)

    NOTE: работает только для моно-цветных изображений (пиксель 0-255)
    NOTE: при написании алгоритмов упор делался на понимание при просмотре логики работы,
        из-за чего код местами не оптимален (по-хорошему нужно сделать документацию
        с принципами работы и отрефакторить). По этой же причине некоторые коэффициенты
        имеют не PEP-8 названия (в стиле констант)

    каталог с дефолтными источниками/результатами:
    C:\Users\Mikhail\PycharmProjects\graphic\static

    Лекции:
    A:\savedata\учёба\7 сем\экз КГ\подготовка\лекции
    """
    STATIC_DIR = os.path.join(os.getcwd(), 'static')
    DEFAULT_TARGET = os.path.join(STATIC_DIR, "barbara.png")
    DEFAULT_SAVE_DIR = os.path.join(os.getcwd(), 'tmp')

    filters = filters_api.filters

    def __init__(self, save_dir=DEFAULT_SAVE_DIR, async=True):
        self.SAVE_DIR = save_dir
        self.ASYNC = async

    def process_image(self, img_filter, target, save_to: str = None, **kwargs):
        """
        Обработка изображения выбранным фильтром

        :param img_filter: экземпляр фильтра из self.filters
        :param target:     путь к исходной картинке
        :param save_to:    путь к новой картинке (если не передано не сохранит)
        :param kwargs:     дополнительные параметры для фильтра (из Filter.addition_kwargs)
        :return:           обработанное изображение
        """
        processed_image = img_filter(target, async=self.ASYNC, **kwargs)
        if save_to:
            processed_image.save(save_to)
        return processed_image


@singleton
class GraphicConsoleUI(GraphicAPI):
    """
    консольный интерфейс для фильтров
    """

    def start(self):
        filters_names = list(self.filters)
        filters_names.sort()
        num_labs = {i+1: img_filter for i, img_filter in enumerate(filters_names)}
        select_text = 'Select filter (input number):'
        for ind, name in num_labs.items():
            select_text += f'\n\t{ind}: {name}'
        select_text += '\n>>> '
        while True:
            try:
                lab = int(input(select_text))
                lab_name = num_labs[lab]
                img_filter = self.filters[lab_name]
            except (KeyError, ValueError):
                print(colored('\n\tInvalid function\n', 'red'))
                continue

            while True:
                target = input('Select image (barbara.png as default):\n>>> ') or self.DEFAULT_TARGET
                if target != self.DEFAULT_TARGET and not os.path.isfile(target):
                    print(colored('\tFile not found', 'red'))
                    continue
                print(colored('\tFile loaded', 'green'))
                break

            kwargs = {}
            for arg in img_filter.addition_kwargs:
                while True:
                    data = input(f'{arg.input_label}:\n>> ')
                    if data.isdigit():
                        data = int(data)
                    if arg.validator(data):
                        if data:
                            kwargs[arg.name] = data
                        break
                    else:
                        print(colored('Некорректное значение', 'red'))

            img_format = target.split('.')[-1]
            result_path = os.path.join(self.SAVE_DIR, f'lab-{lab}.{img_format}')

            try:
                self.process_image(img_filter, target, result_path, **kwargs)
            except FilterError as e:
                print(colored(f'\nSomething went wrong:\n\t{e}\n', 'red'))
            else:
                print(colored(f'\nResult saved at: {result_path}\n\n', 'green'))


@singleton
class GraphicTkinterUI(GraphicAPI):
    """
    десктопный интерфейс для фильтров
    """
    ACCEPTED_FORMATS = (
        ("png files", "*.png"),
        ("jpeg files", "*.jpg"),
        ("tiff files", "*.tiff")
    )

    # размеры для интерфейса
    TARGET_SIZE = (700, 700)
    ADDITIONAL_IMAGE_SIZE = (250, 250)
    RESULT_SIZE = (700, 700)
    LEFT_TABLE_COLUMN_WIDTH = 35

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.target = self.DEFAULT_TARGET
        self.img_filter = None
        self.img_filter_index = None
        self.filter_kwargs = {}

    def __init_grid(self):
        # разметка интерфейса интерфейс
        top = Tk()
        top.title('GraphicAPI')
        top.iconbitmap(os.path.join(self.STATIC_DIR, 'logo.ico'))
        top.resizable(width=FALSE, height=FALSE)
        self.top_level = top

        params_table = LabelFrame(top, text='Filter parametrs')
        params_table.grid(row=0, column=0, padx=5, pady=5, sticky=N+W+S+E)
        self.params_table_left = Frame(params_table)
        self.params_table_left.grid(row=0, column=0, padx=5, pady=5, sticky=N+W)
        self.params_table_right = Frame(params_table)
        self.params_table_right.grid(row=0, column=1, padx=5, pady=5, sticky=N+E)

        self.left_column_bottom = LabelFrame(top, text='Source')
        self.left_column_bottom.grid(row=1, column=0, padx=5, pady=5, sticky=N+W+S+E)

        self.right_column = LabelFrame(top, text='result')
        self.right_column.grid(row=0, rowspan=2, column=1, padx=5, pady=5, sticky=W+S+E)

    @staticmethod
    def get_tk_image(path, size):
        """
        получаем PhotoImage нужного размера для вывода в интерфейсе
        NOTE: тут небольшое "читерство" - ресайзим через PIL
            к фильтрам оно не относится, так что решил взять готовую функцию

        :param path: путь к картинке
        :param size: (int, int)
        :return: ImageTk.PhotoImage
        """
        thumb = Image.open(path)
        thumb.thumbnail(size)
        return ImageTk.PhotoImage(thumb)

    def start(self):
        self.__init_grid()

        # выбор функции
        Label(self.params_table_left, text='Select filter:', width=self.LEFT_TABLE_COLUMN_WIDTH).pack()
        filters = Listbox(self.params_table_left)
        filters_names = list(self.filters)
        filters_names.sort()
        for ind, name in enumerate(filters_names):
            filters.insert(ind+1, name)
        filters.bind('<<ListboxSelect>>', self.__filter_select)
        filters.pack(fill=BOTH)

        # выбор картинки
        Button(
            self.params_table_left,
            text='Привенить фильтр',
            command=self.__process,
            bg="#07c",
            fg="#e4e6e8"
        ).pack(fill=X)

        default_source_img = self.get_tk_image(self.target, self.TARGET_SIZE)
        current_image = Label(self.left_column_bottom, image=default_source_img)
        current_image.pack()
        Button(
            self.left_column_bottom,
            text='Select image (barbara.png as default)',
            command=partial(self.__load_source_file, current_image)
        ).pack(fill=X)

        self.top_level.mainloop()

    def __filter_select(self, evt):
        w = evt.widget
        selection = w.curselection()
        if not selection:
            return
        index = int(selection[0])
        value = w.get(index)
        print('Selected filter %d: "%s"' % (index, value))
        self.img_filter = self.filters[value]
        self.img_filter_index = index
        self.filter_kwargs = {}

        # убираем результат предыдущей фильтрации
        for widget in self.right_column.winfo_children():
            widget.destroy()
        # чистим доп.параметры фильтра
        for widget in self.params_table_right.winfo_children():
            widget.destroy()

        for arg in self.img_filter.addition_kwargs:
            # добавляем пусое введённое значение в filter_kwargs (дефолтное)
            self.filter_kwargs[arg] = ''
            if arg.input_type == INT:
                Label(self.params_table_right,
                      text=f'{arg.input_label}:',
                      width=self.LEFT_TABLE_COLUMN_WIDTH
                      ).pack(fill=X)
                field = Entry(self.params_table_right)
                field.pack(expand=YES, fill=X)
                # для полей ввода заносим само поле, на этапе сборки просто вычисляем
                self.filter_kwargs[arg] = lambda f=field: f.get()
            elif arg.input_type == SELECT:
                label, *options = arg.input_label.split('\t')
                label = label.replace('\n', '')
                options = [o.strip() for o in options]
                Label(self.params_table_right, text=label, width=self.LEFT_TABLE_COLUMN_WIDTH).pack(fill=X)
                param = Listbox(self.params_table_right)
                for ind, name in enumerate(options):
                    param.insert(ind+1, name)
                param.bind('<<ListboxSelect>>', partial(self.__select_filter_arg, arg))
                param.pack(fill=BOTH)
            elif arg.input_type == FILE:
                image_preview = Label(self.params_table_right)
                Label(self.params_table_right, text=arg.input_label, width=self.LEFT_TABLE_COLUMN_WIDTH).pack(fill=X)
                field = Button(
                    self.params_table_right,
                    text='Select',
                    command=partial(self.__file_filter_arg, arg, image_preview)
                ).pack(fill=X)
                image_preview.pack()
            else:
                raise ValueError(arg.input_type)

    def __load_source_file(self, current_image):
        filename = filedialog.askopenfilename(filetypes=self.ACCEPTED_FORMATS)
        if filename:
            self.target = filename
            img = self.get_tk_image(filename, self.TARGET_SIZE)
            current_image.configure(image=img)
            current_image.image = img

    def __select_filter_arg(self, arg, evt):
        w = evt.widget
        selection = w.curselection()
        if not selection:
            return
        index = int(selection[0])
        # в данном случае берём по счёту
        # value = w.get(index)
        self.filter_kwargs[arg] = index + 1  # порядок select-ов в консольном интерфейсе с единицы

    def __file_filter_arg(self, arg, image_preview):
        filename = filedialog.askopenfilename(filetypes=self.ACCEPTED_FORMATS)
        if filename:
            self.filter_kwargs[arg] = filename
            img = self.get_tk_image(filename, self.ADDITIONAL_IMAGE_SIZE)
            image_preview.configure(image=img)
            image_preview.image = img

    def __process(self):
        """
        собирает все данные из формы, если ошибок нет
        вызывает process_image  собранными данными
        """

        # проверка общих аргументов
        if not (self.img_filter and self.target):
            showerror(title="Invalid parametr", message="Select filter and image")
            return

        # добавляем и проверяем дополнительные аргументы фольтра
        process_kwargs = {}
        for arg, value in self.filter_kwargs.items():
            if hasattr(value, '__call__'):
                # вычисляем значения для лямбда-функций
                # получения данных из текстовых полей
                value = value()
            # преобразовываем текстовый ввод в цифру где возможно
            if value and arg.input_type == INT:
                value = int(value)
            if arg.validator(value):
                if value:
                    process_kwargs[arg.name] = value
            else:
                showerror(title="Invalid parametr", message=arg.error_text)
                return

        p = Thread(target=self.__process_image_wrapper,
                   args=(self.img_filter, self.target),
                   kwargs=process_kwargs)
        self.current_process = p
        p.start()

        # убираем результат предыдущей фильтрации
        for widget in self.right_column.winfo_children():
            widget.destroy()

    def __process_image_wrapper(self, *args, **kwargs):
        """
        обёртка с функцией обратного вызова для обработки картинки
        после обработки рендерит результат
        """
        try:
            img_file = self.process_image(*args, **kwargs)
        except FilterError as e:
            showerror(title='Something went wrong', message=str(e))
            return
        img_file.thumbnail(self.RESULT_SIZE)
        result_img = ImageTk.PhotoImage(img_file)
        result_block = Label(self.right_column, image=result_img)
        result_block.image = result_img
        result_block.pack()

        img_ext = self.target.rsplit('.', 1)[-1]
        Button(
            self.right_column,
            text='Сохранить',
            command=partial(self.__save_result, img_file, self.img_filter.name, img_ext),
            bg="#83ff81",
            fg="#0095ff"
        ).pack(fill=X)

    @staticmethod
    def __save_result(img, name, ext):
        path = filedialog.asksaveasfilename(initialfile=name, defaultextension=f'.{ext}')
        if not path:
            return
        img.save(path)
        showinfo(title='Success', message=f'Image was saved:\n{path}')


if __name__ == '__main__':
    # для будущих тестов
    # GraphicAPI.filters
    GraphicAPI(async=False).process_image(GraphicAPI.filters['конвертация в ЧБ-картинку'],
                                          target=r'C:/Users/Mikhail/Pictures/GameCenter/ArcheAge/ArcheAge.jpg')
    GraphicAPI().process_image(GraphicAPI.filters['конвертация в ЧБ-картинку'],
                               target=r'C:/Users/Mikhail/Pictures/GameCenter/ArcheAge/ArcheAge.jpg')
    # GraphicTkinterUI().start()
    # GraphicConsoleUI().start()
    ...
