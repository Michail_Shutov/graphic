from ..base import BaseFilter, FilterAdditionArg, INT, SELECT, MONO
from filters import filters_api
from PIL import Image


class Filter(BaseFilter):
    name = 'размытие'  # сжатие? вес получается меньше, но тут не идёт целью сохранение чёткости
    type = MONO

    addition_kwargs = [
        FilterAdditionArg(
            name='mode',
            input_label='Режим:\n'
                        '\t1 - медиана\n'
                        '\t2 - наращение\n'
                        '\t3 - эррозия',
            input_type=SELECT,
            validator=lambda x: x in range(1, 4),
            error_text='режим обработки не найден'),
        FilterAdditionArg(
            name='ampertude',
            input_label='Ampertude (default core mode)',
            input_type=INT,
            validator=lambda x: not x or isinstance(x, int),
            error_text='ampertude должно быть целым числом')
    ]

    def get_worker_kwargs(self, source, mode=1, ampertude=None):
        """
        :param source: ImageData исходного изображения
        :param mode:
            1 - медиана
            2 - наращение
            3 - эррозия)
        :param ampertude: ampertude (по дефолту работает с ядром)
        """

        size = (source.width, source.height)
        image = Image.new('L', size)

        core = None
        if not ampertude:
            core = [
                [1, 2, 1],
                [2, 3, 2],
                [1, 2, 1]
            ]

            print('\nCore:\n')
            for i in range(len(core)):
                print(core[i])

            ampertude = len(core)

        return image, {'ampertude': ampertude, 'core': core, 'mode': mode}

    def worker(self, i, j, pix, source, ampertude, core, mode):
        mediana = []
        try:
            for j1 in range(ampertude):
                for i1 in range(ampertude):
                    if core:
                        # добавляем эл-т кол-во раз зажанное в ядре
                        mediana.extend([source.pix[i + i1, j + j1]] * core[j1][i1])
                    else:
                        mediana.append(source.pix[i + i1, j + j1])
            mediana.sort()
            if mode == 1:
                if len(mediana) % 2 != 0:
                    ind = int(len(mediana) / 2) + 1 - 1
                    pix[i, j] = mediana[ind]
                else:
                    ind_1 = int(len(mediana) / 2 - 1)
                    ind_2 = int(len(mediana) / 2 + 1 - 1)
                    pix[i, j] = int((mediana[ind_1] + mediana[ind_2]) / 2)
            if mode == 2:
                pix[i, j] = mediana[-1]
            if mode == 3:
                pix[i, j] = mediana[0]

        except IndexError:
            pass
            # полутонавая эррозия
            # полутоновое наращение
            # (максимальный и минимальный фильтр)

filters_api.register(Filter)
