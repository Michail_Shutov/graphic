from ..base import BaseFilter, MONO
from filters import filters_api


class Filter(BaseFilter):
    name = 'выравнивание гистограммы'
    type = MONO

    def get_worker_kwargs(self, source):
        image = source.file.copy()

        hist = image.histogram()
        pix_count = source.width * source.height
        # 1 вар - минимальное и максимальное значение яркости, количество пикселей которой превышает порога
        # 2 вар - количество пиксалей светлее/темнее порога

        # на первый взгляд было бы логичней сделать нахождение s1 и s2 в 2-x а не 4-х с половиной функциях,
        # НО при текущем подходе намного проще отслеживать изменения пороговых значений
        # s1 и s2 при изменении их процентных порогов
        print('\n', '-' * 40)
        # тут первый:
        procent_h = 0.004
        porog_h = int(pix_count * procent_h)
        print(f'\nПорог H составляет {procent_h} от общего числа пикселей',
              f'\nВсего пикселей: {pix_count}',
              f'\nПороговое значение: {porog_h}')

        i = 0
        for hist_el in hist:
            if hist_el > porog_h:
                s1_1 = i
                break
            else:
                i += 1

        fl = True
        i = 255
        while fl:
            if hist[i] > porog_h:
                s2_1 = i
                fl = False
            else:
                i -= 1
            if i < 0:
                fl = False

        print('\nНайденные по 1 способу:'
              '\nS1: ', s1_1,
              '\nS2: ', s2_1)

        # тут второй:
        procent_c = 0.01
        porog_c = int(pix_count * procent_c)
        print(f'\nПорог C составляет {procent_c} от общего числа пикселей',
              f'\nВсего пикселей: {pix_count}',
              f'\nПороговое значение: {porog_c}')

        amount = 0
        i = 0
        for hist_el in hist:
            amount += hist_el
            if amount > porog_c:
                s1_2 = i
                break
            else:
                i += 1

        fl = True
        i = 255
        amount = 0
        while fl:
            amount += hist[i]
            if amount > porog_c:
                s2_2 = i
                fl = False
            else:
                i -= 1
            if i < 0:
                fl = False

        print('\nНайденные по 2 способу:'
              '\nS1: ', s1_2,
              '\nS2: ', s2_2)

        s1 = min(s1_1, s1_2)
        s2 = min(s2_1, s2_2)

        print('\n', '-' * 40)
        print('\nS1:', s1, '\nS2: ', s2, '\n')

        # при отклонении от единицы становится светлее\темнее
        gamma = 1
        return image, {'s1': s1, 's2': s2, 'gamma': gamma}

    def worker(self, i, j, pix, source, s1, s2, gamma):
        if pix[i, j] < s1:
            pix[i, j] = 0
        else:
            if pix[i, j] > s2:
                pix[i, j] = 255
            else:
                pix[i, j] = int(int((pix[i, j] - s1) / (s2 - s1) * 255) ** gamma)

filters_api.register(Filter)
