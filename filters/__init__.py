import importlib

from glob import glob
from singleton_decorator import singleton


@singleton
class FiltersAPI(object):
    def __init__(self):
        self._filters = {}
        self.loaded = False

    def register(self, cls):
        obj = cls()
        self._filters[obj.name] = obj

    @property
    def filters(self):
        if not self.loaded:
            self.load()
        return self._filters

    def as_list(self):
        return self.filters.values()

    def load(self):
        for path_to_module in glob(f'{__package__}/*/filter.py'):
            try:
                importlib.import_module(path_to_module.replace('\\', '.').replace('.py', ''))
            except ImportError as e:
                print(e)
                pass
        self.loaded = True


filters_api = FiltersAPI()
