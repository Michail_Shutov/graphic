from ..base import BaseFilter, MONO
from filters import filters_api
from PIL import Image


class Filter(BaseFilter):
    name = 'негатив'
    type = MONO

    # K = 1/5
    K = 1

    def get_worker_kwargs(self, source):
        size = (source.width, source.height)
        image = Image.new('L', size)

        # matrix = [
        #     [ 0, 1, 0 ],
        #     [ 1, 1, 1 ],
        #     [ 0, 1, 0 ],
        # ]
        matrix = [
            [1, 1, 1],
            [1, -8, 1],
            [1, 1, 1]
        ]

        amount = 0
        for i in range(len(matrix)):
            for j in range(len(matrix)):
                matrix[i][j] *= self.K
                amount += matrix[i][j]

        delta = (len(matrix) - 1) / 2
        return image, {'matrix': matrix, 'delta': delta}

    def worker(self, i, j, pix, source, matrix, delta):
        pix[i, j] = 0
        try:
            amount = 0
            for j1 in range(len(matrix)):
                for i1 in range(len(matrix)):
                    amount += source.pix[i - delta + i1, j - delta + j1] * matrix[j1][i1]
            if amount < 0:
                amount = 0
            if amount > 255:
                amount = 255
            pix[i, j] = int(amount)
        except IndexError:
            pass

filters_api.register(Filter)
