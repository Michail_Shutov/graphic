from ..base import BaseFilter, FilterAdditionArg, INT, MONO
from filters import filters_api
from PIL import Image


class Filter(BaseFilter):
    name = 'интерполяция (увеличение)'
    type = MONO

    addition_kwargs = [
        FilterAdditionArg(
            name='zoom',
            input_label='Уровень увеличения',
            input_type=INT,
            validator=lambda x: isinstance(x, int),
            error_text='Не корректное значение уровня увеличения. Ожидается целое число')
    ]

    @staticmethod
    def __interpolation(x, y, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16):
        b1 = ((x - 1) * (x - 2) * (x + 1) * (y - 1) * (y - 2) * (y + 1)) / 4
        b2 = -1 * (x * (x + 1) * (x - 2) * (y - 1) * (y - 2) * (y + 1)) / 4
        b3 = -1 * (y * (x - 1) * (x - 2) * (x + 1) * (y + 1) * (y - 2)) / 4
        b4 = (x * y * (x + 1) * (x - 2) * (y + 1) * (y - 2)) / 4

        b5 = -1 * (x * (x - 1) * (x - 2) * (y - 1) * (y - 2) * (y + 1)) / 12
        b6 = -1 * (y * (x - 1) * (x - 2) * (x + 1) * (y - 1) * (y - 2)) / 12
        b7 = (x * y * (x - 1) * (x - 2) * (y + 1) * (y - 2)) / 12
        b8 = (x * y * (x + 1) * (x - 2) * (y - 1) * (y - 2)) / 12

        b9 = (x * (x - 1) * (x + 1) * (y - 1) * (y - 2) * (y + 1)) / 12
        b10 = (y * (x - 1) * (x - 2) * (x + 1) * (y - 1) * (y + 1)) / 12
        b11 = (x * y * (x - 1) * (x - 2) * (y - 1) * (y - 2)) / 36
        b12 = -1 * (x * y * (x - 1) * (x + 1) * (y + 1) * (y - 2)) / 12

        b13 = -1 * (x * y * (x + 1) * (x - 2) * (y - 1) * (y + 1)) / 12
        b14 = -1 * (x * y * (x - 1) * (x + 1) * (y - 1) * (y - 2)) / 36
        b15 = -1 * (x * y * (x - 1) * (x - 2) * (y - 1) * (y + 1)) / 36
        b16 = (x * y * (x - 1) * (x + 1) * (y - 1) * (y + 1)) / 36

        return int(b1 * a1 + b2 * a2 + b3 * a3 + b4 * a4 + b5 * a5 + b6 * a6 + b7 * a7 + b8 * a8 +
                   b9 * a9 + b10 * a10 + b11 * a11 + b12 * a12 + b13 * a13 + b14 * a14 + b15 * a15 + b16 * a16)

    def get_worker_kwargs(self, source, zoom=2):
        size = (source.width * zoom, source.height * zoom)
        image = Image.new('L', size)

        new_width, new_height = image.size
        print('width:  %s -> %s' % (source.width, new_width))
        print('height: %s -> %s' % (source.height, new_height))
        return image, {'zoom': zoom}

    def worker(self, i, j, pix, source, zoom):
        for j1 in range(zoom):
            for i1 in range(zoom):
                try:
                    pix[i * zoom + i1, j * zoom + j1] = self.__interpolation(
                        i1 / zoom, j1 / zoom,
                        source.pix[i, j], source.pix[i, j + 1], source.pix[i + 1, j], source.pix[i + 1, j + 1],
                        source.pix[i, j - 1], source.pix[i - 1, j], source.pix[i + 1, j - 1], source.pix[i - 1, j + 1],
                        source.pix[i, j + 2], source.pix[i + 2, j], source.pix[i - 1, j - 1], source.pix[i + 1, j + 2],
                        source.pix[i + 2, j + 1], source.pix[i - 1, j + 2], source.pix[i + 2, j - 1],
                        source.pix[i + 2, j + 2])
                except IndexError:
                    pass

filters_api.register(Filter)
