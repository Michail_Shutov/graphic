import random
import os

from ..base import BaseFilter, FilterAdditionArg, FILE, MONO
from filters import filters_api
from PIL import Image


class GaussFilter(BaseFilter):
    name = 'фильтр Гаусса'
    type = MONO

    GAUSS = 10
    m = 0
    s = 1

    def get_worker_kwargs(self, source):
        size = (source.width, source.height)
        image = Image.new('L', size)
        return image, {}

    def worker(self, i, j, pix, source):
        pix[i, j] += int(source.pix[i, j] + self.GAUSS * random.normalvariate(self.m, self.s))


class ImpulsFilter(BaseFilter):
    name = 'импульсный фильтр'
    type = MONO

    IMP = 40

    def get_worker_kwargs(self, source):
        size = (source.width, source.height)
        image = Image.new('L', size)
        return image, {}

    def worker(self, i, j, pix, source):
        rand = random.randint(0, self.IMP)
        if rand == 0:
            pix[i, j] = 0
        else:
            if rand == 1:
                pix[i, j] = 255
            else:
                pix[i, j] = source.pix[i, j]


class BlendFilter(BaseFilter):
    name = 'смешение двух картинок'
    type = MONO

    addition_kwargs = [
        FilterAdditionArg(
            name='second_image',
            input_label='Select second image (baboon.png as default)',
            input_type=FILE,
            validator=lambda f: not f or os.path.isfile(f),
            error_text='файл не найден')
    ]

    IMG_DIR = os.path.join(
        os.path.abspath(__package__.replace('.', '/')),
        'static')
    DEFAULT_SECOND_TARGET = os.path.join(IMG_DIR, "baboon.png")

    def __create_alfa_channel(self, file_name, strength, size):
        img = Image.new('L', size)
        pix = img.load()
        width, height = size
        for i in range(width):
            for j in range(height):
                pix[i, j] = strength
        img.save(os.path.join(self.IMG_DIR, file_name))

    def get_worker_kwargs(self, source, second_image=DEFAULT_SECOND_TARGET):
        size = (source.width, source.height)
        image = Image.new('L', size)

        # создаём альфа-каналы для первой и второй картинки
        self.__create_alfa_channel("ALPHA_1.png", 100, size)
        self.__create_alfa_channel("ALPHA_2.png", 60, size)

        # открываем изображения альфа-каналов
        alpha_1 = Image.open(os.path.join(self.IMG_DIR, "ALPHA_1.png"))
        alpha_1_pix = alpha_1.load()
        alpha_2 = Image.open(os.path.join(self.IMG_DIR, "ALPHA_2.png"))
        alpha_2_pix = alpha_2.load()

        # открываем вторую картинку (немного "читерим" приводя её размер, но фильтр не про это)
        second_img = Image.open(second_image).resize(size, Image.ANTIALIAS)
        source2 = self.get_img_data(second_img)
        return image, {'source2': source2, 'alpha_1_pix': alpha_1_pix, 'alpha_2_pix': alpha_2_pix}

    def worker(self, i, j, pix, source, source2, alpha_1_pix, alpha_2_pix):
        # IMG_1 - alpha_1_pix[i,j] / 100 - нижнее 1
        # IMG_2 - alpha_2_pix[i,j] / 100 - верхнее 0.8
        cb = source.pix[i, j] / 255
        cs = source2.pix[i, j] / 255
        blend_res = cs
        # blend_res = 1 - ( ( 1- cb )*( 1 - cs ) )
        tmp1 = (1 - alpha_2_pix[i, j] / 100) * (alpha_1_pix[i, j] / 100) * (source.pix[i, j] / 255)
        tmp2 = (1 - alpha_1_pix[i, j] / 100) * (alpha_2_pix[i, j] / 100) * (source2.pix[i, j] / 255)
        tmp3 = (alpha_1_pix[i, j] / 100) * (alpha_2_pix[i, j] / 100) * blend_res
        pix[i, j] = int((tmp1 + tmp2 + tmp3) * 255)


filters_api.register(GaussFilter)
filters_api.register(ImpulsFilter)
filters_api.register(BlendFilter)
