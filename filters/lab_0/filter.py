from ..base import BaseFilter, FilterError, RGB
from filters import filters_api
from PIL import Image


class Filter(BaseFilter):
    name = 'конвертация в ЧБ-картинку'
    type = RGB

    def get_worker_kwargs(self, source):
        size = (source.width, source.height)
        image = Image.new('L', size)
        return image, {}

    def worker(self, i, j, pix, source):
        r, g, b = source.pix[i, j]
        s = (r + g + b) // 3
        pix[i, j] = s

filters_api.register(Filter)
