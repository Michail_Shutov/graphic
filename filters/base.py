import itertools
import numpy

from abc import ABCMeta, abstractmethod
from collections import namedtuple
from typing import List
from multiprocessing import cpu_count
from PIL import Image


# Filter.type
MONO = 'mono'
RGB = 'rgb'
ALL = 'all'

# FilterAdditionArg.input_type
INT = 'int'
FILE = 'file'
SELECT = 'select'


class FilterError(Exception):
    pass


# предзагруженные данные изображения
ImageData = namedtuple('ImageData', [
    'file',   # PIL.Image
    'pix',    # image.load()
    'width',  # ширина
    'height'  # высота
])


# дополнительный аргумент фильтра
FilterAdditionArg = namedtuple('FilterAdditionArg', [
    'name',         # имя_переменной
    'input_label',  # фраза_для_запроса_ввода
    'input_type',   # тип_инпута: INT | FILE | SELECT
                    #   для select формат: зашоловок [\n вариант]+
                    #   разбиение по переносу строки, на выход число,
                    #   соответствующее порядку элемента (с единицы)
    'validator',    # функция проверки
    'error_text'    # выводотся при провале валидатора
])


def _tracer(func):
    def wrapper(*args, **kwargs):
        import time
        start = time.clock()
        result = func(*args, **kwargs)
        print(f'{func.__name__} was finished at {time.clock() - start}')
        return result
    return wrapper

class BaseFilter(metaclass=ABCMeta):
    """
    функция обработки изображения разбита на 2 этапа:
      1) get_worker_kwargs - формарование параметров для воркера
      2) worker - обработка воркером
    Это позволяет использовать асинхронную обработку изображений
    """

    # добавляются в kwargs при вызове __call__
    # используются для параллельной реализации интерфейсов апи /
    #   независимого использования фильтров после импорта /
    #   удобного тестирования
    addition_kwargs: List[FilterAdditionArg] = []

    @property
    @abstractmethod
    def name(self) -> str:
        """
        :return: имя фильтра для использования
        """
        pass

    @property
    @abstractmethod
    def type(self) -> str:
        """
        :return: тип фильтра MONO / RGB / ALL
        """
        pass

    @abstractmethod
    def get_worker_kwargs(self, source, **kwargs):
        """
        Получает список дополнительных параметров для воркера
        :param source: ImageData исзодного изображения
        :param kwargs: дополнительные параметры фильтра (определяются через FilterAdditionArg)
        :return:       (заготовка_новой_картинки, словарь_с_аргументами_вызова_воркера)
        """

    @abstractmethod
    def worker(self, i, j, pix, source, **kwargs):
        """
        обработчик фильтра pix[i, j]
        :param i: позиция по width
        :param j: позиция по height
        :param pix: агруженные пиксели исходного изображения
        :param source: ImageData исходного изображения
        :param kwargs: полученные self.get_worker_kwargs аргументы
        :return: None
        """

    @classmethod
    def get_img_data(cls, f, pix=None):
        """
        Формирует ImageData из картинки и проверяет на возможность работы
        фильтра с данным типом изображением
        :param f: PIL.Image instance
        :param pix: numpy-массив пикселей (для обработки с многопоточностью)
        :return:  ImageData
        """
        img_data = ImageData(
            file=f,
            width=f.size[0],
            height=f.size[1],
            pix=pix if pix is not None else f.load()
        )
        test_pix = img_data.pix[0, 0]
        if cls.type == RGB:
            if not ((isinstance(test_pix, tuple) or isinstance(test_pix, numpy.ndarray)) and len(test_pix) == 3):
                raise FilterError('Selected image is not in RGB-format')
        elif cls.type == MONO:
            if not (isinstance(test_pix, int) or isinstance(test_pix, numpy.int32)):
                raise FilterError('Selected image is not in L-format')
        return img_data

    @_tracer
    def process(self, path, **kwargs):
        """одновоточная обработка"""
        source = self.get_img_data(Image.open(path))
        image, worker_kwargs = self.get_worker_kwargs(source, **kwargs)
        pix = image.load()
        for j in range(source.height):
            for i in range(source.width):
                self.worker(i, j, pix, source, **worker_kwargs)
        return image

    @_tracer
    def process_async(self, path, **kwargs):
        """попытки организовать многопоточные вычисления"""
        from multiprocessing import Queue, Process

        s_file = Image.open(path)
        s_width, s_height = s_file.size
        s_pix = list(s_file.getdata())

        s_pix = numpy.array([s_pix[i * s_width:(i + 1) * s_width]
                             for i in range(s_height)],
                            dtype=numpy.int
                            ).swapaxes(0, 1)
        source = self.get_img_data(Image.open(path), s_pix)

        image, worker_kwargs = self.get_worker_kwargs(source, **kwargs)
        new_width, new_height = image.size
        new_pix = list(image.getdata())
        new_pix = numpy.array([new_pix[i * new_width:(i + 1) * new_width]
                               for i in range(new_height)],
                              dtype=numpy.int
                              ).swapaxes(0, 1)

        processes = cpu_count()
        worker_args = (new_pix, source)
        result_queue = Queue()

        pool = []
        ranges = self.get_ranges(source.height, processes)
        for r in ranges:
            p = Process(target=self.mp_worker,
                        args=(r, source.width,
                              worker_args, worker_kwargs,
                              result_queue))
            pool.append(p)
            p.start()

        result = []
        for i in range(len(pool)):
            result.append(result_queue.get())

        result = sorted(result, key=lambda x: x[0][0])

        flat_pix_res = []
        for (start, stop), pix in result:
            pix = numpy.array(pix).swapaxes(0, 1)
            [flat_pix_res.extend(l) for l in pix[start:stop]]
            # list(itertools.chain.from_iterable(x))
            # arr.flatten()

        image.putdata(flat_pix_res)
        return image

    def __call__(self, path, async=True, **kwargs):
        image = self.process_async(path, **kwargs) if async else self.process(path, **kwargs)

        '''
        варианты на основе массивов numpy
        
        https://stackoverflow.com/questions/37863967/pil-apply-the-same-operation-to-every-pixel
        
        # быстрая обработка изображений на готовых фильтрах scipy + numpy
        http://www.scipy-lectures.org/advanced/image_processing/
        '''
        return image

    def mp_worker(self, current_range, width, worker_args, worker_kwargs, results_queue):
        print(f'worker start: {current_range}')
        for j in range(*current_range):
            for i in range(width):
                self.worker(i, j, *worker_args, **worker_kwargs)
        results_queue.put((current_range, worker_args[0]))  # list of pixels

    @staticmethod
    def get_ranges(length, process_count):
        """
        Получает интервалы для обработки списка
        длинной length
        количеством ядер count
        :param length: длинна списка
        :param process_count: сколько срезов нужно (по кол-ву процессов)
        :return: список интервалов
        """
        result = []
        adder = 1 if length % process_count else 0
        step = length // process_count + adder
        ranges = list(range(0, length, step))
        ranges_len = len(ranges)
        for i in range(ranges_len):
            start = ranges[i]
            stop = ranges[i + 1] if i + 1 < ranges_len else length
            result.append((start, stop))
        return result
