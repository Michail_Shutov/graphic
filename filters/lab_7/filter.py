from ..base import BaseFilter, ALL
from filters import filters_api
from PIL import Image, ImageDraw


class Filter(BaseFilter):
    name = '???'
    type = ALL

    @staticmethod
    def __target_replace(x, y, target):
        for i in range(len(target)):
            for j in range(len(target[0])):
                if target[i][j] == x:
                    target[i][j] = y

    def get_worker_kwargs(self, source):

        target = [
            [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0],
            [0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1],
            [0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
            [0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
            [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0],
            [0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0],
            [0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1],
            [0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ]

        print('\nTARGET:\n')
        for string in target:
            print(string)

        n = len(target[0])
        print('\nWidth: ', n)
        m = len(target)
        print('\nHeight: ', m)

        km = 0
        kn = 0
        cur = 1

        counter = 0

        for i in range(m):
            for j in range(n):

                kn = j - 1
                if kn < 0:
                    kn = 1
                    B = 0
                else:
                    B = target[i][kn]

                km = i - 1
                if km < 0:
                    km = 1
                    C = 0
                else:
                    C = target[km][j]

                A = target[i][j]

                if A == 0:
                    pass
                elif B == 0 and C == 0:
                    cur += 1
                    counter += 1
                    target[i][j] = cur
                elif B != 0 and C == 0:
                    target[i][j] = B
                elif B == 0 and C != 0:
                    target[i][j] = C
                elif B != 0 and C != 0:
                    if B == C:
                        target[i][j] = B
                    else:
                        target[i][j] = B
                        print(C, ' -> ', B)
                        self.__target_replace(C, B, target)
                        counter -= 1

        print('\nRESULT:\n')
        for string in target:
            print(string)

        print('\nCounter:', counter)

        # хз что этот фильтр делает, походу считал что-то для лаборанта. пусть лежит пока тут
        # по факту он вообще с исходной картинкой не работает
        # return image

        return None, {}

    def worker(self, i, j, pix, source):
        ...

# TODO: вспомнить что конкретно делает данный фильтр, адаптировать под текущую версию
# filters_api.register(Filter)
