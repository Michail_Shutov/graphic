from ..base import BaseFilter, MONO
from filters import filters_api


class Filter(BaseFilter):
    name = 'добавление шумов (зернистость)'
    type = MONO

    def get_worker_kwargs(self, source, **kwargs):
        image = source.file.copy()
        return image, {}

    def worker(self, i, j, pix, source):
        if pix[i, j] <= 127:
            error = pix[i, j]
            pix[i, j] = 0
        else:
            error = pix[i, j] - 255
            pix[i, j] = 255

        error_1 = int(error * 8 / 42)
        error_2 = int(error * 4 / 42)
        error_3 = int(error * 2 / 42)
        error_4 = int(error * 4 / 42)
        error_5 = int(error * 8 / 42)
        error_6 = int(error * 4 / 42)
        error_7 = int(error * 2 / 42)
        error_8 = int(error * 1 / 42)
        error_9 = int(error * 2 / 42)
        error_10 = int(error * 4 / 42)
        error_11 = int(error * 2 / 42)
        error_12 = int(error * 1 / 42)

        # попадаем в IndexError когда выходим за границы матрицы
        # моджно было бы сделать проверку на края,
        # но с ней выглядит не так наглядно что делаем
        try:
            pix[i, j + 1] += error_1
        except IndexError:
            pass
        try:
            pix[i, j + 2] += error_2
        except IndexError:
            pass
        try:
            pix[i + 1, j - 2] += error_3
        except IndexError:
            pass
        try:
            pix[i + 1, j - 1] += error_4
        except IndexError:
            pass
        try:
            pix[i + 1, j] += error_5
        except IndexError:
            pass
        try:
            pix[i + 1, j + 1] += error_6
        except IndexError:
            pass
        try:
            pix[i + 1, j + 2] += error_7
        except IndexError:
            pass
        try:
            pix[i + 2, j - 2] += error_8
        except IndexError:
            pass
        try:
            pix[i + 2, j - 1] += error_9
        except IndexError:
            pass
        try:
            pix[i + 2, j] += error_10
        except IndexError:
            pass
        try:
            pix[i + 2, j + 1] += error_11
        except IndexError:
            pass
        try:
            pix[i + 2, j + 2] += error_12
        except IndexError:
            pass

filters_api.register(Filter)
